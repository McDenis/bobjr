**Configuration globale de Git**
git config --global user.name "[NAME]"
git config --global user.email "[EMAIL]"

**Créer un nouveau dépôt**
git clone git@gitlab.com:McDenis/bobjr.git
cd bobjr
git switch --create main
touch README.md
git add README.md
git commit -m "add README"
git push --set-upstream origin main

**Pousser un dossier existant**
cd existing_folder
git init --initial-branch=main
git remote add origin git@gitlab.com:McDenis/bobjr.git
git add .
git commit -m "Initial commit"
git push --set-upstream origin main

**Pousser un dépôt Git existant**
cd existing_repo
git remote rename origin old-origin
git remote add origin git@gitlab.com:McDenis/bobjr.git
git push --set-upstream origin --all
git push --set-upstream origin --tags
