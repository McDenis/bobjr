const fs = require('fs');

function LogErreur(error) {
    var d = new Date()
    fs.appendFileSync('./erreurs/erreurs.log', `[${d.toLocaleString()}] - Error : ${error}\n`)
}

function ExecuteSimpleQuery(bobdb, sQuery) {
    bobdb.query(sQuery, (err) => {
        if (err) {  LogErreur(err)  } 
    })
}

function ExecuteResultQuery(bobdb, sQuery) {
    return new Promise((resolve) => {
        bobdb.query(sQuery, (err, result) => {
            if (err) { LogErreur(err) } 
            else {
                if (result.length > 0) { resolve(result) }
                else { resolve(false) }
            }
        })
    })
}

function CheckUser(bobdb, userID) {
    sQuery = `SELECT * FROM Users WHERE IDUser = ${userID};`

    return new Promise((resolve) => {
        bobdb.query(sQuery, (err , result) => {
            if (err) { LogErreur(err)} 
            else {
                if (result.length > 0) { resolve(true) }
                else { resolve(false) }
            }
        })
    })
}

function CheckBeast(bobdb, userID) {
    sQuery = `SELECT * FROM UserBeast WHERE IDUser = ${userID}`

    return new Promise((resolve) => {
        bobdb.query(sQuery, (err , result) => {
            if (err) { LogErreur(err)} 
            else {
                if (result.length > 0) { resolve(true) }
                else { resolve(false) }
            }
        })
    })
}

function CreateUser(bobdb, userID, username, bot) {
    if (!bot) {
        sQuery = `INSERT INTO Users (IDUser, Username) VALUES (${userID}, '${username}')`
        bobdb.query(sQuery, (err) => {
            if (err) { LogErreur(err) } 
        })
    }
}

function addExperience(bobdb, userID, xpToAdd) {
    sQuery = `SELECT * FROM UserBeast WHERE IDUser = ${userID};`
    
    return new Promise((resolve, reject) => {
        bobdb.query(sQuery, (err, result) => {
            if (err) { LogErreur(err) ; reject(err) } 
            else {
                if (result.length > 0) { 
                    if (result[0].Exp + xpToAdd >= result[0].NextLevel) {
                        TotalRemaining = (result[0].Exp + xpToAdd) - result[0].NextLevel
                        
                        // sQuery = `UPDATE UserBeast SET Level = Level + 1, Exp = ${TotalRemaining}, NextLevel = NextLevel * 2 WHERE IDUser = ${userID}`
                        // bobdb.query(sQuery, (err) => {
                        //     if (err) { logErreur(err) ; reject(err) } 
                        // })

                        resolve(true)
                    }
                    else {
                        sQuery = `UPDATE UserBeast SET Exp = Exp + ${xpToAdd} WHERE IDUser = ${userID};`
                        
                        bobdb.query(sQuery, (err) => {
                            if (err) { logErreur(err) } 
                            else { resolve(false) }
                        })
                    }
                }
                else { resolve(false) }
            }
        })
    })
}

function GenerateRandomStat(min, max){
    return Math.floor(Math.random() * (max - min + 1)) + min
}

function GenerateStats(nHealth, nAttack, nDefense, nCritical) {
    MinHealth = nHealth - 2 ; MaxHealth = nHealth + 2
    MinAttack = nAttack - 2 ; MaxAttack = nAttack + 2
    MinDefense = nDefense - 2 ; MaxDefense = nDefense + 2
    MinCritical = nCritical - 2 ; MaxCritical = nCritical + 2

    if (MinAttack <= 0) { MinAttack = 1 } ; if (MaxAttack > 10) { MaxAttack = 10 }
    if (MinDefense <= 0) { MinDefense = 1 } ; if (MaxDefense > 5) { MaxDefense = 5 }
    if (MinCritical <= 0) { MinCritical = 1 } ; if (MaxCritical > 5) { MaxCritical = 5 } 
  
    return {
        Health: GenerateRandomStat(MinHealth, MaxHealth),
        Attack: GenerateRandomStat(MinAttack, MaxAttack),
        Defense: GenerateRandomStat(MinDefense, MaxDefense),
        Critical: GenerateRandomStat(MinCritical, MaxCritical),
    };
}

function CalculateDamage(attack, critical) {
    return attack * (1 + critical / 100)
}

module.exports = {
    LogErreur: LogErreur,
    ExecuteSimpleQuery: ExecuteSimpleQuery,
    ExecuteResultQuery: ExecuteResultQuery, 
    CheckUser: CheckUser,
    CheckBeast: CheckBeast,
    CreateUser: CreateUser,
    addExperience: addExperience,
    GenerateRandomStat: GenerateRandomStat,
    GenerateStats: GenerateStats,
    CalculateDamage: CalculateDamage,
}