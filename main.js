const { Client, Intents, MessageEmbed } = require('discord.js')

const mysql = require('mysql')
const auth = require('./auth.json')
const fonctions = require('./functions/functions.js')
const fs = require('fs')

const client = new Client({ 
    intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.GUILD_MESSAGE_REACTIONS, Intents.FLAGS.GUILD_MEMBERS, Intents.FLAGS.GUILD_VOICE_STATES]
})

// Create connexion to database
const bobdb = mysql.createConnection({
    host: 'localhost',
    user: 'bob',
    password: 'mrbob',
    database: 'bobjr'
})

// On discord display "PLAYING ?help"
client.on('ready', function (evt) {
    console.log(`Logged in as ${client.user.tag}!`);
    client.user.setPresence({ 
        activities: [{
            name: "?help",
            type: 'PLAYING', // COMPETING - CUSTOM - LISTENING - PLAYING - STREAMING - WATCHING
        }],
        status: 'online'
    })
})

// Conncetion to database
bobdb.connect(function(err) {
    if (err) { fonctions.LogErreur(err) } 
    else { console.log('Connected to BobJr database!') }
})

// We create all profiles to avoid problems with orders
client.on('ready', async (client) => {
    const guild  = client.guilds.cache.get('396061280609370112'); 
    let res = await guild.members.fetch()
   
    res.forEach((member) => {
        fonctions.CheckUser(bobdb, member.user.id)
        .then(result => {
            if (result === false) {
                fonctions.CreateUser(bobdb, member.user.id, member.user.username, member.user.bot)
                
                if (!member.user.bot) { console.log(`Creating of ${member.user.username}'s profile`) }
            }
        })
        .catch(error => {
            fonctions.LogErreur(error)
            return
        })  
    })
})

// For each commande
client.on('messageCreate', async message => {
    // Check if the message from a bot
    if (message.author.bot) return

    if (message.content.startsWith('?')) {
        // Arguments and the command
        const args = message.content.slice(1).trim().split(/ +/)
        const cmd = args.shift().toLowerCase()

        // Embed for messages
        const ConstantEmbed = new MessageEmbed()
            .setColor('0x00298c')
            .setThumbnail(client.user.displayAvatarURL)

        // Check if the user have a Beast
        if ((cmd !== "create") && (cmd !== "help")) {
            fonctions.CheckBeast(bobdb, message.author.id)
            .then(result => {
                if (result === false) {
                    message.channel.send({
                        embeds: [
                            ConstantEmbed.setTitle('No beast found')
                            .setDescription(`You need to create your beast first !
                            Start with ?create`)
                        ]
                    })
                    return
                }
            })
            .catch(error => {
                fonctions.LogErreur(error)
                return
            })
        }

        // All commands
        try{
            switch(cmd) {
                /********************************* BobJr HELP ***********************************/
                case 'help':
                    HelpP1 = fs.readFileSync(`./messages/help_1.txt`, 'utf8')
                        
                    message.react('✅')
                    message.author.send({ 
                        embeds: [ 
                            ConstantEmbed.setTitle(`BobJr is here to help`)
                            .addFields(
                                { name: `Commands :`, value: `${HelpP1}` },
                                // { name: '_ _', value: `${HelpP1}` },
                                // { name: '_ _', value: `${HelpP1}` }
                            )
                            .setDescription('')  
                        ] 
                    })
                break
                /****************************************************************************************/

                /********************************* BobJr STARTING ***********************************/
                case 'create':
                    // Create the beast
                    message.channel.send({
                        embeds: [
                            ConstantEmbed.setTitle('Name your beast')
                            .setDescription(`What is the name of your beast ?`)
                        ]
                    })

                    const filter = m => m.author.id === message.author.id
                    const collector = message.channel.createMessageCollector({ filter, time: 30000, max: 1 })

                    // User response
                    collector.on('collect', m => {
                        const BeastName = m.content
                        if (BeastName.trim() === "") {
                            message.channel.send({
                                embeds: [
                                    ConstantEmbed.setTitle(`Name error`)
                                    .setDescription(`Your beast's name cannot be empty`)
                                ]
                            })
                            return
                        }

                        // Health - Attack - Defense - Critical
                        const Health    = fonctions.GenerateRandomStat(5, 10) // Between 5 and 10
                        const Attack    = fonctions.GenerateRandomStat(2, 5)  // Between 1 and 3
                        const Defense   = fonctions.GenerateRandomStat(1, 3)  // Between 1 and 3
                        const Critical  = fonctions.GenerateRandomStat(1, 3)  // Between 2 and 5 
                        

                        // Send a message with Beast data
                        message.channel.send({
                            embeds: [
                                ConstantEmbed.setTitle(`${BeastName}`)
                                .setDescription(`__Your Beast profile :__
                                Health : ${Health} :heart:
                                Attack : ${Attack} :crossed_swords:
                                Defense : ${Defense} :shield:
                                Critical : ${Critical} :boom:`) 
                            ]
                        })

                        // Insert into database
                        fonctions.ExecuteSimpleQuery(bobdb, `INSERT INTO UserBeast (IDBeast, IDUser, BeastName, Health, Attack, Defense, Critical) VALUES
                        (1, ${message.author.id}, '${BeastName}', ${Health}, ${Attack}, ${Defense}, ${Critical})`)
                    })

                    // No user response
                    collector.on('end', collected => {
                        if (collected.size === 0) {
                            message.channel.send({
                                embeds: [
                                    ConstantEmbed.setTitle(`Time error`)
                                    .setDescription(`No response received within 30 seconds`)
                                ]
                            })
                        }
                    })
                break
                /****************************************************************************************/

                /********************************* BobJr PROFILES ***********************************/
                case 'profile':
                case 'p':
                    // Profile of the beast
                    let UserIDProfil
                    
                    if (args[1] === undefined || args[1] === null || args[1] === '') { UserIDProfil = message.author.id } else { UserIDProfil = args[1].replace(/[<@!>]/g, "") }
                    targetUser = client.users.cache.get(UserIDProfil)

                    const resultProfil = await fonctions.ExecuteResultQuery(bobdb, `SELECT * FROM UserBeast WHERE IDUser = ${UserIDProfil}`)

                    TotalParties  = resultProfil[0].Win + resultProfil[0].Draw + resultProfil[0].Lost
                    WinRate = ((2 * resultProfil[0].Win) + resultProfil[0].Draw) / (2 * TotalParties) * 100

                    if (isNaN(WinRate)) { WinRate = 0 }

                    message.channel.send({
                        embeds: [
                            ConstantEmbed.setAuthor({ name: `${targetUser.username}'s Beast profile`, iconURL: `${targetUser.avatarURL()}`, url: '' })
                            .setDescription(`
                            **Name :** ${resultProfil[0].BeastName}
                            _ _
                            :heart: ${resultProfil[0].Health}\u00A0\u00A0\u00A0\u00A0 :crossed_swords:${resultProfil[0].Attack}
                            :shield: ${resultProfil[0].Defense}\u00A0\u00A0\u00A0\u00A0 :boom: ${resultProfil[0].Critical}
                            _ _
                            **Number of games :** ${TotalParties}
                            **Winrate :** :trophy: ${WinRate.toFixed(2)}% 
                            _ _
                            **Fights Won :** <:win:1079117243759218699> ${resultProfil[0].Win}
                            **Fights Draw :** <:draw:1079118690743750697> ${resultProfil[0].Draw}
                            **Fights Lost :** :x: ${resultProfil[0].Lost}
                            `)
                        ]
                    })
                break

                // Beast experience profile
                case 'xp':
                    let UserIDExp
                    if (args[1] === undefined || args[1] === null || args[1] === '') { UserIDExp = message.author.id } else { UserIDExp = args[1].replace(/[<@!>]/g, "") }
                    
                    const resultExp = await fonctions.ExecuteResultQuery(bobdb, `SELECT * FROM UserBeast WHERE IDUser = ${UserIDExp}`)
                    targetUser = client.users.cache.get(UserIDExp)

                    ExpNextLevel = resultExp[0].NextLevel - resultExp[0].Exp
                    UserProgress = Math.round((resultExp[0].Exp * 100 / resultExp[0].NextLevel), 2)

                    if (isNaN(UserProgress)) { UserProgress = 0}

                    EmojiProgress = '<:5pc:1106881992273973370>'
                    if (UserProgress >= 25 && UserProgress < 50) { EmojiProgress = '<:25pc:1106881994341761065>' } 
                    else if (UserProgress >= 50 && UserProgress < 90) { EmojiProgress = '<:50pc:1106881995566481568>' }
                    else if (UserProgress >= 90) { EmojiProgress = '<:90pc:1106881996883509320>' }

                    message.channel.send({
                        embeds: [
                            ConstantEmbed.setAuthor({ name: `${targetUser.username}'s Beast experience`, iconURL: `${targetUser.avatarURL()}`, url: '' })
                            .setDescription(`
                            **Current level :** <:level:1106875098075508756> ${resultExp[0].Level}

                            **Experience :** <:exp:1106876766422835261> ${resultExp[0].Exp} / ${resultExp[0].NextLevel}
                            **Required :** <:xpup:1106879182228701187> ${ExpNextLevel}
                            
                            **Progression :** ${EmojiProgress} ${UserProgress} %`)
                        ]
                    })
                break
                /****************************************************************************************/

                /********************************* BobJr UTILITY ***********************************/
                case 'fight':
                    // Fight against an other beast
                    const BeastOpponentNames = ['BrutalMaster','FuriousFist','WildThunder','SeismicStrike','BrutalBlade','CombativeFury','DevastatingShadow','FierceColossus','SavageStrike','BrutalBreaker','RuthlessGriffin','UnleashedStorm','UnstoppableRage','BrawlTitan','BrutalHammer','SeismicShock','FuriousFlare','WildArena','BoneCrusher','FearlessBrute']
                    const OpponentName = BeastOpponentNames[Math.floor(Math.random() * BeastOpponentNames.length)]
                    
                    const PlayerStats = await fonctions.ExecuteResultQuery(bobdb, `SELECT * FROM UserBeast WHERE IDUser = ${message.author.id}`)
                    const OpponentStats = fonctions.GenerateStats(PlayerStats[0].Health, PlayerStats[0].Attack, PlayerStats[0].Defense, PlayerStats[0].Critical)

                    message.channel.send({
                        embeds: [
                            ConstantEmbed.setTitle(`:chart_with_upwards_trend: Beasts statistics`)
                            .setDescription(`**__${PlayerStats[0].BeastName}__**
                            :heart: ${PlayerStats[0].Health}\u00A0\u00A0\u00A0\u00A0 :crossed_swords:${PlayerStats[0].Attack}
                            :shield: ${PlayerStats[0].Defense}\u00A0\u00A0\u00A0\u00A0 :boom: ${PlayerStats[0].Critical}
                            _ _
                            <:versus:1195309283160629279>
                            _ _
                            **__${OpponentName}__**
                            :heart: ${OpponentStats.Health}\u00A0\u00A0\u00A0\u00A0 :crossed_swords:${OpponentStats.Attack}
                            :shield: ${OpponentStats.Defense}\u00A0\u00A0\u00A0\u00A0 :boom: ${OpponentStats.Critical}
                            `)
                        ]
                    })
                    
                    let Turn = 1                        
                    let BeastHealth = PlayerStats[0].Health

                    while (BeastHealth > 0 && OpponentStats.Health > 0) {
                        await delay(500)                      

                        // Calculate damage inflicted by player and opponent
                        const PlayerDamage = fonctions.CalculateDamage(PlayerStats[0].Attack, PlayerStats[0].Critical)
                        const OpponentDamage = fonctions.CalculateDamage(OpponentStats.Attack, OpponentStats.Critical)

                        // Apply the critic
                        const PlayerCritMultiplier = 1 + (PlayerStats[0].Critical / 100) // Critical damage multiplier
                        const OpponentCritMultiplier = 1 + (OpponentStats.Critical / 100)

                        const PlayerTotalDamage = Math.floor(PlayerDamage * PlayerCritMultiplier)
                        const OpponentTotalDamage = Math.floor(OpponentDamage * OpponentCritMultiplier)

                        // Percentage chance of dodging
                        const PlayerDodge = PlayerStats[0].Defense * 10
                        const OpponentDodge = OpponentStats.Defense * 10

                        // Check whether fighters are dodging damage
                        const PlayerDodged = Math.random() * 100 < PlayerDodge
                        const OpponentDodged = Math.random() * 100 < OpponentDodge

                        const PlayerFinalDamage = PlayerDodged ? 0 : PlayerTotalDamage
                        const OpponentFinalDamage = OpponentDodged ? 0 : OpponentTotalDamage

                        BeastHealth -= OpponentFinalDamage;
                        OpponentStats.Health -= PlayerFinalDamage;

                        Turn++
                    }

                    let NewLevel
                   
                    // Simulate the outcome of the fight
                    if (BeastHealth > 0) {
                        message.channel.send({
                            embeds: [
                                ConstantEmbed.setTitle(`:medal: You win !`)
                                .setDescription(`**${PlayerStats[0].BeastName}** beat the **${OpponentName}**'s beast !`)
                            ]
                        })

                        fonctions.ExecuteSimpleQuery(bobdb, `UPDATE UserBeast SET Win = Win + 1 WHERE IDUser = ${message.author.id}`)
                        NewLevel = await fonctions.addExperience(bobdb, message.author.id, 5)
                    } 
                    else if (BeastHealth <= 0 && OpponentStats.Health > 0) {
                        message.channel.send({
                            embeds: [
                                ConstantEmbed.setTitle(`:skull: You lose !`)
                                .setDescription(`**${OpponentName}** beat your beast **${PlayerStats[0].BeastName}**`)
                            ]
                        })

                        fonctions.ExecuteSimpleQuery(bobdb, `UPDATE UserBeast SET Lost = Lost + 1 WHERE IDUser = ${message.author.id}`)
                        NewLevel = await fonctions.addExperience(bobdb, message.author.id, 1)
                    } 
                    else {
                        message.channel.send({
                            embeds: [
                                ConstantEmbed.setTitle(`:flag_white: Draw !`)
                                .setDescription(`Your beast and your opponent's beast didn't succeed in this fight`)
                            ]
                        })

                        fonctions.ExecuteSimpleQuery(bobdb, `UPDATE UserBeast SET Draw = Draw + 1 WHERE IDUser = ${message.author.id}`)
                        NewLevel = await fonctions.addExperience(bobdb, message.author.id, 3)                        
                    }

                    if (NewLevel) {
                        const BeastMax = await fonctions.ExecuteResultQuery(bobdb, `SELECT * FROM UserBeast WHERE IDUser = ${message.author.id}`)
                        let ComponentsArray = [
                            {
                                type: 1,
                                components: [ { type: 2, style: 'SECONDARY', custom_id: 'health', label: `+1 ❤️`, } ]
                            },
                        ]

                        if (BeastMax[0].Attack <= 10) {
                            ComponentsArray[0].components.push({ type: 2, style: 'SECONDARY', custom_id: 'attack', label: `+1 ⚔️`, },)
                        }
                        if (BeastMax[0].Defense <= 5) {
                            ComponentsArray[0].components.push({ type: 2, style: 'SECONDARY', custom_id: 'defense', label: `+1 🛡️`, })
                        }
                        if (BeastMax[0].Critical <= 5) {
                            ComponentsArray[0].components.push({ type: 2, style: 'SECONDARY', custom_id: 'critical', label: `+1 💥`, })
                        }

                        const NewMessage = await message.channel.send({
                            components: ComponentsArray
                        })

                        const filter = button => {
                            return button.user.id === message.author.id
                        }

                        const button = await NewMessage.awaitMessageComponent({ filter: filter, componentType: 'BUTTON', max: 1 })
                        NewMessage.delete()

                        if (button.customId === 'health') { 
                            sQuery = `UPDATE UserBeast SET Level = Level + 1, Exp = 0, NextLevel = NextLevel * 2, Health = Health + 1 WHERE IDUser = ${message.author.id}` 
                            sMessage = `Your beast received +1 :heart:`
                        }
                        if (button.customId === 'attack') { 
                            sQuery = `UPDATE UserBeast SET Level = Level + 1, Exp = 0, NextLevel = NextLevel * 2, Attack = Attack + 1 WHERE IDUser = ${message.author.id}` 
                            sMessage = `Your beast received +1 :crossed_swords:`
                        }
                        if (button.customId === 'defense') { 
                            sQuery = `UPDATE UserBeast SET Level = Level + 1, Exp = 0, NextLevel = NextLevel * 2, Defense = Defense + 1 WHERE IDUser = ${message.author.id}` 
                            sMessage = `Your beast received +1 :shield:`
                        }
                        if (button.customId === 'critical') { 
                            sQuery = `UPDATE UserBeast SET Level = Level + 1, Exp = 0, NextLevel = NextLevel * 2, Critical = Critical + 1 WHERE IDUser = ${message.author.id}` 
                            sMessage = `Your beast received +1 :boom:`
                        }
                        
                        const BeastStats = await fonctions.ExecuteResultQuery(bobdb, `SELECT * FROM UserBeast WHERE IDUser = ${message.author.id}`)

                        return button.channel.send({
                            embeds: [
                                ConstantEmbed.setTitle(`<:xpup:1106879182228701187> New attribut`)
                                .setDescription(`${sMessage}
                                _ _
                                :heart: ${BeastStats[0].Health}\u00A0\u00A0\u00A0\u00A0 :crossed_swords:${BeastStats[0].Attack}
                                :shield: ${BeastStats[0].Defense}\u00A0\u00A0\u00A0\u00A0 :boom: ${BeastStats[0].Critical}
                                `)
                            ]
                        })   
                    }
                break
                /****************************************************************************************/
                
                /********************************* BobJr INFORMATIONS ***********************************/
                case 'fb':
                case 'feedback':
                    fonctions.addExperience(bobdb, msg.author.id, 10)

                    let argValue = args[0]
                    let argLenght = argValue.toString().length + 1

                    Idea = message.content.substring(argLenght)
                    Name = message.author.username

                    message.delete()

                    message.channel.send({
                        embeds: [ 
                            exampleEmbed.setTitle('Feedback')
                            .addFields(
                                { name: `Proposal sent`, value: `Thanks for your help. Your message will be analyzed`, inline: true }
                            )
                            .setDescription('') 
                        ]
                    })

                    client.users.cache.get('497087235460694038').send({
                        embeds: [
                            exampleEmbed.setTitle(`${Name}'s Feedback`)
                            .setFields(
                                { name: 'Information : ', value: Idea, inline: true }
                            )
                            .setDescription('') 
                        ]
                    })
                break

                case 'version':
                case 'v':
                    message.delete()
                
                    message.channel.send({
                        embeds: [ 
                            exampleEmbed.setTitle('BobJr Version')
                            .setDescription('I am currently in ' + fs.readFileSync('./version.txt', 'utf8')) 
                        ]
                    })
                break
                /****************************************************************************************/
            }
        }
        catch(err){
            fonctions.LogErreur(`${err.message}`)
        }
    }
})

function delay(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

// Token
client.login(auth.token)
